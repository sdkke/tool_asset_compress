//压缩目标目录上级不能有 'sdk_tmp' 文件夹

import Tools from "./tools/tools";

const fs = require("fs");
const path = require('path');
const child_process = require('child_process');

export default class index {
    // private static assetUrl = path.join(__dirname, '../../assets');
    private static assetUrl = path.join(__dirname, './src');
    private static tempName = './sdk_tmp/';//临时目录的名字
    private static tempUrl = '';
    private static pngArr = [];
    private static jpgArr = [];
    private static mp3Arr = [];
    private static errorArr = [];

    public static async run(assetUrl?) {

        this.clearData();

        //如果有传值，就用传值过来的路径，否则全部压缩
        if (assetUrl) {
            this.logInfo(`选中的压缩路径：${assetUrl}`);
            await this.readFileList(assetUrl);
        } else {
            this.logInfo(`未选中压缩目录，使用默认目录：${this.assetUrl}`);
            await this.readFileList(this.assetUrl);
        }
        await Tools.init();

        // 同步执行exec
        child_process.execPromise = (cmd, options, callback) => {
            return new Promise<void>((resolve, reject) => {
                child_process.exec(cmd, options, (err, stdout, stderr) => {
                    if (err) {
                        reject(err);
                    }
                    resolve();
                })
            });
        };

        // let url = __dirname + '/tmp';
        let url = path.join(this.tempUrl, this.tempName);
        //创建临时文件夹
        await fs.exists(url, function (exists) {
            if (!exists) {
                fs.mkdirSync(url);
            }
        });
        await this.compressPng();
        this.logInfo('png压缩完毕');
        await this.compressJpg();
        this.logInfo('jpg/jpeg压缩完毕');
        await this.compressMp3();
        this.logInfo('mp3压缩完毕');
        //删除临时文件
        this.deleteAll(url);
        this.logInfo('资源全部压缩完毕');

        this.logInfo('错误次数' + this.errorArr.length);
        for (let i = 0;i < this.errorArr.length;i++) {
            this.logInfo(`错误${i + 1}:${this.errorArr[i]}`);
        }


    }

    private static clearData() {
        this.pngArr = [];
        this.jpgArr = [];
        this.mp3Arr = [];
        this.errorArr = [];
    }

    /**
     * 递归检测出所有资源
     * @param url 目标路径
     */
    private static readFileList(url) {
        const stat = fs.statSync(url);
        this.tempUrl = path.join(url, '../');
        if (stat.isDirectory()) {
            // this.logInfo('文件夹');
            let files = fs.readdirSync(url, "utf-8");
            files = files.filter((file) => {
                return !file.endsWith(".meta");
            });
            files.forEach((item, index) => {
                var fullPath = path.join(url, item);
                const stat = fs.statSync(fullPath);
                if (stat.isDirectory()) {
                    //如果文件夹内还包含文件夹，那么继续读取这个文件夹内的内容
                    this.readFileList(path.join(url, item));  //递归读取文件
                } else {
                    this.detectionType(fullPath);
                }
            });
        } else {
            // this.logInfo('文件');
            this.detectionType(url);
            return;
        }
    }

    //检测文件类型
    private static detectionType(url) {
        if (url.endsWith(".png")) {
            this.pngArr.push(url);
        } else if (url.endsWith(".jpg") || url.endsWith(".jpeg")) {
            // var fullPath = path.join(__dirname, item);
            this.jpgArr.push(url);
        } else if (url.endsWith(".mp3")) {
            this.mp3Arr.push(url);
        }
    }


    private static async compressPng() {
        for (let i = 0;i < this.pngArr.length;i++) {
            // let file = pngArr[i];

            let currentPath = this.pngArr[i];
            let targetPath = path.join(this.tempUrl, this.tempName) + path.basename(this.pngArr[i]);
            // 参数文档： https://pngquant.org/
            let quality = '65-80'; // 图像质量
            // let cmd = `${Tools.pngquant} -f --ext .png  --quality ${quality} "${file}"`;
            let cmd = `${Tools.pngquant} --transbug --force 256 --output "${targetPath}" --quality ${quality} "${currentPath}"`;
            if (cmd) {
                await child_process.execPromise(cmd).then(() => {
                    this.moveFile(targetPath, currentPath);
                }).catch((err) => {
                    this.errorArr.push(currentPath);
                    this.logInfo(err);
                });
                this.logInfo(`${currentPath} 压缩完成，${i + 1}/${this.pngArr.length}`);
            }
        }
    }

    private static async compressJpg() {

        for (let i = 0;i < this.jpgArr.length;i++) {

            let currentPath = this.jpgArr[i];
            let targetPath = path.join(this.tempUrl, this.tempName) + path.basename(this.jpgArr[i]);
            // 参数文档： https://linux.die.net/man/1/jpegtran
            let cmd = `${Tools.jpegtran} -copy none -optimize -perfect -progressive -outfile "${targetPath}" "${currentPath}"`;

            if (cmd) {
                await child_process.execPromise(cmd).then(() => {
                    this.moveFile(targetPath, currentPath);
                }).catch((err) => {
                    this.errorArr.push(currentPath);
                    this.logInfo(err);
                });
                this.logInfo(`${currentPath} 压缩完成，${i + 1}/${this.jpgArr.length}`);
            }
        }
    }

    // 压缩mp3
    private static async compressMp3() {
        for (let i = 0;i < this.mp3Arr.length;i++) {
            let currentPath = this.mp3Arr[i];
            let targetPath = path.join(this.tempUrl, this.tempName) + path.basename(this.mp3Arr[i])
            let cmd = `${Tools.lame} -V 0 -q 0 -b 45 -B 80 --abr 64 "${currentPath}" "${targetPath}"`;
            await child_process.execPromise(cmd).then(() => {
                this.moveFile(targetPath, currentPath);
            }).catch((err) => {
                this.errorArr.push(currentPath);
                this.logInfo(err);
            });
            this.logInfo(`${currentPath} 压缩完成，${i + 1}/${this.mp3Arr.length}`);
        }
    }

    /**
     * 移动文件
     * @param sourceFile 要移动的文件
     * @param destPath 要覆盖的文件
     */
    private static async moveFile(sourceFile, destPath) {
        let one = fs.statSync(sourceFile).size;
        let two = fs.statSync(destPath).size;
        if (one < two) {
            await fs.rename(sourceFile, destPath, async (err) => {
                if (err)
                    this.logInfo(err);
                // throw err;
                await fs.stat(destPath, (err, stats) => {
                    if (err)
                        this.logInfo(err);
                    // throw err;
                });
            });
        }
    }


    private static deleteAll(path) {
        var files = [];
        if (fs.existsSync(path)) {
            files = fs.readdirSync(path);
            files.forEach(function (file, index) {
                var curPath = path + "/" + file;
                if (fs.statSync(curPath).isDirectory()) { // recurse
                    this.deleteAll(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    };

    private static logInfo(str) {
        try {
            //让tsc编译的时候 忽略Editor 这个错误
            //@ts-ignore
            Editor.log(str);
        } catch (error) {
            console.log(str);
        }
    }
}
let url = process.argv[2];
index.run(url);
