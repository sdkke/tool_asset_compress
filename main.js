"use strict";
exports.__esModule = true;
var index = require("./index");

module.exports = {
  load() {
    // 当 package 被正确加载的时候执行
  },

  unload() {
    // 当 package 被正确卸载的时候执行
  },

  messages: {
    creator() {
      let assets = Editor.Selection.curSelection("asset");
      if (assets.length != 0) {
        let selectPath = Editor.assetdb.uuidToFspath(assets).replace(/\\/g, "\/");
        index["default"].run(selectPath);
      } else {
        index["default"].run();
      }
    }
  },
};